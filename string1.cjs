function string1(str){

    let stringSize = str.length;
    let result;
    let negative = false;

    if(str[0] =='-'){
        negative = true;
    }

    for(let stringIndex = 0; stringIndex < stringSize; stringIndex++)
    {
        if(str[stringIndex] == ',' || str[stringIndex] == '$'){
            str = str.replace(str[stringIndex],'');
        }
    }

    result = parseFloat(str);
   
    return result;
}
module.exports = string1;