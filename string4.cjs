function string4(obj){

    let firstName = obj.first_name;
    let middleName = obj.middle_name;
    let lastName = obj.last_name;

    let resultName  ='';

    if(typeof firstName != 'undefined'){

        firstName = firstName[0].toUpperCase();
        for(let stringIndex = 1; stringIndex < obj.first_name.length; stringIndex++){
            firstName += obj.first_name[stringIndex].toLowerCase();
        }
        resultName += firstName;
        resultName += ' ';
    }

    if(typeof middleName != 'undefined'){

        middleName = middleName[0].toUpperCase();
        for(let stringIndex = 1; stringIndex < obj.middle_name.length; stringIndex++){
            middleName += obj.middle_name[stringIndex].toLowerCase();
        }
        resultName += middleName;
        resultName += ' ';
    }
    if(typeof lastName != 'undefined'){
        
        lastName = lastName[0].toUpperCase();
        for(let stringIndex = 1; stringIndex < obj.last_name.length; stringIndex++){
            lastName += obj.last_name[stringIndex].toLowerCase();
        }
        resultName += lastName;
    }
    return resultName;
    

}
module.exports = string4;