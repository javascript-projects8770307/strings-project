function string5(stringArr){

    if(stringArr.length == 0){
        return [];
    }

    let resultString = '';
    for(let stringIndex = 0; stringIndex <stringArr.length; stringIndex++){
        resultString += stringArr[stringIndex];
        
        if(stringIndex != stringArr.length -1){
            resultString += ' ';
        }
    }
    resultString += '.';
    
    return resultString;

    
}
module.exports = string5;