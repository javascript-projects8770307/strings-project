function string3(str){

    var month = {
        1:"January",
        2:"February",
        3:"March",
        4:"April",
        5:"May",
        6:"June",
        7:"July",
        8:"August",
        9:"September",
        10:"October",
        11:"November",
        12:"December"
    }
    
    let stringArray = str.split('/');
    let monthString = parseInt(stringArray[1]);

    if(monthString < 1 || monthString > 12){
        return "Invalid Month";
    }

    let result = month[monthString];
    return result;
    
}
module.exports = string3;