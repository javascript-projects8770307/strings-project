function string2(str){
    if(str == ''){
        return [];
    }

    let resultString = str.split('.');
    
    for(let stringIndex = 0; stringIndex < resultString.length; stringIndex++){
        resultString[stringIndex] = parseInt(resultString[stringIndex]);
    }

    return resultString;
    
}
module.exports = string2;